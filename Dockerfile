FROM ubuntu:22.04 AS base

RUN set -e

ENV DEBIAN_FRONTEND=noninteractive

# Install basic packages
RUN apt-get -y update
RUN apt-get install -y --no-install-recommends\
	python3\
	python3-pip\
	make\
	curl\
	cmake\
	clang\
	git

# copy/clone bubaak
FROM base AS packages
RUN apt-get install -y --no-install-recommends llvm llvm-dev
# LEE dependencies and optional packages
RUN apt-get install -y --no-install-recommends ncurses-dev libsqlite3-dev libz3-dev zlib1g-dev libgoogle-perftools-dev libc6-dev-i386
# SlowBeast dependencies and optional packages
RUN pip install pyyaml lit numpy

# copy and setup Bubaak
FROM packages AS bubaak
WORKDIR /opt

# copy here, change for the line below if you want to clone from a repo
COPY . /opt/bubaak
#RUN git clone --depth=1 https://gitlab.com/mchalupa/bubaak

WORKDIR /opt/bubaak
RUN git clean -xdf
RUN git submodule foreach "git clean -xdf"
# These are steps performed by `make setup`.
# We break them here down so that if a build fails, we still got cached
# previous steps that have succeeded and do not need to redo the whole
# `make setup`
RUN make slowbeast-llvmlite
RUN make build-klee-uclibc -j4
RUN make build-klee -j4

