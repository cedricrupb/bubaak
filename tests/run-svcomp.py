#!/usr/bin/env python3

import os
import sys
import glob
import yaml

from subprocess import run, STDOUT
from shutil import copyfile
from concurrent.futures import ThreadPoolExecutor as PoolExecutor
from multiprocessing import cpu_count


TIMEOUT = 4
NPROC = int(cpu_count() / 3)

if len(sys.argv) < 3 or not sys.argv[1].endswith(".prp"):
    print("Expect: file.prp file.set [file.set ...]")
    sys.exit(1)

PRP_ARG = sys.argv[1]

DIR = os.path.abspath(os.path.dirname(sys.argv[0]))
OUTDIR = f"{DIR}/outputs"
WITTNESSDIR = f"{DIR}/witnesses"
BUBAAK = os.path.abspath(f"{DIR}/../bubaak")
REPORT_FILES_DIR = f"{DIR}/"

os.makedirs(OUTDIR, exist_ok=True)
os.makedirs(WITTNESSDIR, exist_ok=True)

from tempfile import TemporaryFile


def get_files_handle(setfiles):
    """
    Merge all files in .prp files into a temporary file.
    This ensures that we do not overwrite the .prp files
    at the same time as reading them while executing the experiments.
    """
    tmpfile = TemporaryFile(
        mode="w+t",
        encoding="utf-8",
    )
    for setfilename in setfiles:
        setdir = os.path.dirname(setfilename)
        with open(setfilename, "r") as setfile:
            list_files = glob.iglob
            for line in setfile:
                line = line.strip()
                if not line or line.startswith("#"):
                    continue
                absline = line if line[0] == "/" else f"{setdir}/{line}"
                for f in list_files(absline):
                    tmpfile.write(f)
                    tmpfile.write("\n")
    tmpfile.flush()
    tmpfile.seek(0)
    return tmpfile


def get_files(handle):
    for line in handle:
        yield line.rstrip()


def get_verdict(outname):
    with open(outname, "r") as output:
        for line in output:
            if "SV-COMP verdict" in line:
                return line.split(maxsplit=2)[2].strip()
    return "ERROR"


def get_yaml_data(filepath):
    with open(filepath, "r") as stream:
        spec = yaml.load(stream, Loader=yaml.BaseLoader)
    return spec


def store_verdict(verdict, expected_verdict, filepath):
    if verdict.startswith("false"):
        if expected_verdict.startswith("true"):
            outfile = f"{REPORT_FILES_DIR}/false-incorrect.set"
        else:
            outfile = f"{REPORT_FILES_DIR}/false.set"
    elif verdict.startswith("true"):
        if expected_verdict.startswith("false"):
            outfile = f"{REPORT_FILES_DIR}/true-incorrect.set"
        else:
            outfile = f"{REPORT_FILES_DIR}/true.set"
    elif verdict.lower().startswith("unknown"):
        outfile = f"{REPORT_FILES_DIR}/unknown.set"
    elif verdict.lower().startswith("timeout"):
        outfile = f"{REPORT_FILES_DIR}/timeout.set"
    elif verdict.lower().startswith("error"):
        outfile = f"{REPORT_FILES_DIR}/error.set"
    elif verdict == "<no run>":
        outfile = f"{REPORT_FILES_DIR}/noprp.set"
    else:
        outfile = f"{REPORT_FILES_DIR}/other.set"

    with open(outfile, "a") as out:
        out.write(f"{filepath}\n")


def print_verdict(verdict, expected_verdict):
    RED = "\033[31;1m"
    GREEN = "\033[32;1m"
    BLUE = "\033[34;1m"
    PURPLE = "\033[35;1m"
    PURPLE_U = "\033[35;4m"
    color = "\033[0m"
    if verdict.startswith("false"):
        if expected_verdict.startswith("false"):
            color = GREEN
        elif expected_verdict.startswith("true"):
            color = RED
    elif verdict.startswith("true"):
        if expected_verdict.startswith("true"):
            color = GREEN
        elif expected_verdict.startswith("false"):
            color = RED
    elif verdict.lower().startswith("unknown"):
        color = PURPLE_U
    elif verdict.lower().startswith("timeout"):
        color = BLUE
    else:
        color = PURPLE

    print(f"{color}{verdict}\033[0m")


def run_on_file(filepath):
    filename = os.path.basename(filepath)
    expected_prpfilename = os.path.basename(PRP_ARG)

    spec = get_yaml_data(filepath)
    for prp in spec["properties"]:
        prpfilename = os.path.basename(prp["property_file"])
        if prpfilename != expected_prpfilename:
            continue

        outdir = f"{OUTDIR}/{prpfilename}"
        witnessdir = f"{WITTNESSDIR}/{prpfilename}"
        os.makedirs(outdir, exist_ok=True)
        os.makedirs(witnessdir, exist_ok=True)

        outname = f"{outdir}/{filename}.out"
        witnessname = f"{witnessdir}/{filename}.graphml"

        # print(f"RUNNING '{filename}' for {prpfilename}")
        prpfilepath = f"{os.path.dirname(filepath)}/{prp['property_file']}"
        with open(outname, "w") as output:
            cmd = [
                BUBAAK,
                "-timeout",
                str(TIMEOUT),
                "-sv-comp",
                "-sv-comp-witness",
                witnessname,
                "-prp",
                prpfilepath,
                filepath,
            ]
            output.write(f"{' '.join(cmd)}\n\n")
            output.flush()

            proc = run(cmd, stderr=STDOUT, stdout=output)
            output.write(f"\nProcess returned {proc.returncode}\n")

            verdict = get_verdict(outname)
            return filepath, prp, verdict
    return filepath, None, "<no run>"


def doit():
    print(f"--- Starting the tests (using {NPROC} runners) ---")
    with get_files_handle(sys.argv[2:]) as files_handle:
        with PoolExecutor(max_workers=NPROC) as pool:
            for filepath, prp, verdict in pool.map(
                run_on_file, get_files(files_handle)
            ):
                filename = os.path.basename(filepath)
                prpfilename = (
                    os.path.basename(prp["property_file"]) if prp else "<no prp>"
                )
                expected_verdict = prp["expected_verdict"] if prp else "unknown"

                if prp:
                    print(
                        f"DONE '\033[34m{filename:<30}\033[0m' for {prpfilename}",
                        end=" ... ",
                    )
                    print_verdict(verdict, expected_verdict)
                store_verdict(verdict, expected_verdict, filepath)
                sys.stdout.flush()


if __name__ == "__main__":
    try:
        doit()
    except KeyboardInterrupt:
        print("Interrupted...")
        pass
    except Exception as e:
        raise e
    finally:
        pass
        # if proc is not None:
        #    proc.kill()
