from bbk.dbg import print_stdout


def result_kind_to_str(val):
    if val == Result.CORRECT:
        return "CORRECT"
    if val == Result.INCORRECT:
        return "INCORRECT"
    if val == Result.UNKNOWN:
        return "UNKNOWN"
    if val == Result.ERROR:
        return "ERROR"
    if val == Result.TIMEOUT:
        return "TIMEOUT"
    raise RuntimeError(f"Unknown result code: {val}")


class Result:
    CORRECT = 1
    INCORRECT = 2
    UNKNOWN = 3
    ERROR = 4
    TIMEOUT = 5

    def __init__(self, kind, prp, info: str = "", witness=None):
        """
        @param ty is from the enum
        """
        self._kind = kind
        self._prp = prp
        self._info = info
        self._witness = witness

    def is_correct(self):
        return self._kind == Result.CORRECT

    def is_incorrect(self):
        return self._kind == Result.INCORRECT

    def is_unknown(self):
        return self._kind == Result.UNKNOWN

    def is_error(self):
        return self._kind == Result.ERROR

    def is_timeout(self):
        return self._kind == Result.TIMEOUT

    def prp(self):
        return self._prp

    def info(self):
        return self._info

    def witness(self):
        return self._witness

    def __repr__(self):
        return f"Result({result_kind_to_str(self._kind)}, {self._prp}, {self._witness}, '{self._info[:20]}')"

    def describe(self):
        def prp_key(prp):
            return prp.key() if prp else "_any_"

        if self.is_incorrect():
            print_stdout(
                f"Property {prp_key(self.prp())} is violated\n{self.info() or ''}",
                color="red",
            )
        elif self.is_correct():
            print_stdout(
                f"Property {prp_key(self.prp())} holds\n{self.info() or ''}",
                color="green",
            )
        else:
            print_stdout(
                f"Validity of property {prp_key(self.prp())} is unknown\n{self.info() or ''}",
                color="orange",
            )
