from subprocess import Popen, PIPE
from .dbg import print_stderr


def err(msg):
    print_stderr(msg, color="RED")
    exit(1)


def _popen(cmd, env=None):
    try:
        p = Popen(cmd, stdout=PIPE, stderr=PIPE, env=env)
    except FileNotFoundError as e:
        print_stderr(str(e), color="red")
        return None
    return p
