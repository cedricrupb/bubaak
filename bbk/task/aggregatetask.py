from time import clock_gettime, CLOCK_REALTIME
from .task import Task
from bbk.dbg import dbg, wdbg


class AggregateTask(Task):
    """
    This task aggregates results from other tasks.
    It takes their results as they come and decides whether to
    finish or continue the tasks and what to return.

    If this task has a timeout and it is passed, the subtasks
    are stopped too.
    TODO: we could do this configurable
    """

    def __init__(self, tasks, aggregate=None, timeout=None, name=None, descr=None):
        super().__init__(timeout=timeout, name=name, descr=descr)
        if aggregate:
            self.aggregate = aggregate

        self._initial_tasks = tasks

    def execute(self):
        assert self._workflow is not None
        for task in self._initial_tasks:
            self.add_subtask(task)

    def add_subtask(self, task):
        wdbg().msg(f"{self}.add_subtask({task})", color="blue")
        assert self._workflow is not None

        self.listen_to(task)
        self._workflow.add_task(task)

    def subtask_finished(self, task, result):
        wdbg().msg(f"{self}.subtask_finished({task}, {result})", color="blue")
        task.remove_listener(self)
        # We already have the result, therefore the current call of this method
        # is for the stopped tools and we want to do nothing
        if self._result:
            return

        self._result = self.aggregate(task, result)
        # stop all subtasks if we have a result
        if self._result is not None:
            # store into result that `task` is the one that finished
            self._result.subtask = task
            # stop other subtasks
            self.stop()

    def finish(self):
        return self._result

    def is_aggregate(self):
        return True

    def aggregate(self, task, result):
        raise RuntimeError("This method must be overriden or given in __init__")

    def is_running(self):
        return len(self.listens_to()) > 0 and self._start_time is not None

    def stop(self):
        for task in self.listens_to():
            task.stop()

    def kill(self):
        for task in self.listens_to():
            task.kill()

    def is_done(self):
        return not self.listens_to() and self._start_time is not None
