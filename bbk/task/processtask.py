from signal import SIGINT, SIGKILL

from .task import Task
from .result import TaskResult

from bbk.dbg import dbgv, dbg, print_stdout
from bbk.env import get_env
from bbk.utils import _popen


class ProcessResult:
    def __init__(self, retval, sigval=None):
        self.retval = retval
        self.signal = sigval


class ProcessOutputParser:
    def __init__(self, store_stdout=None, store_stderr=None):
        """
        Parameters:
          store_stdout: str, opened file, or None: store stdout to the given file
          store_stderr: str, opened file, or None: store stderr to the given file
        """
        self._log_stdout = store_stdout
        self._log_stderr = store_stderr

    def log_stdout(self, line):
        if self._log_stdout is None:
            return

        # In ctor we store only the path and open it lazily only now.
        # The main reason is that the tools may overwrite or clean
        # the directory where the log is going to be on their
        # startup and this way we'll create the file only after the
        # tool were initialized
        if isinstance(self._log_stdout, str):
            self._log_stdout = open(self._log_stdout, "w")

        self._log_stdout.write(line)
        self._log_stdout.write("\n")

    def log_stderr(self, line):
        if self._log_stderr is None:
            return

        # In ctor we store only the path and open it lazily only now.
        # The main reason is that the tools may overwrite or clean
        # the directory where the log is going to be on their
        # startup and this way we'll create the file only after the
        # tool were initialized
        if isinstance(self._log_stderr, str):
            self._log_stderr = open(self._log_stderr, "w")

        self._log_stderr.write(line)
        self._log_stderr.write("\n")

    def __del__(self):
        if self._log_stdout and hasattr(self._log_stdout, "close"):
            self._log_stdout.close()
        if self._log_stderr and hasattr(self._log_stderr, "close"):
            self._log_stderr.close()
        del self

    def parse(self, line: bytes, stream: str):
        """
        Called for each line of the tool's output.
        @param stream is either "stdout" or "stderr".
        If something else None is returned,
        it is assumed to be the TaskResult()
        """
        line_s = line.decode("utf-8", "ignore")
        if stream == "stdout":
            self.log_stdout(line_s)
            return self._parse_stdout(line_s.strip())
        elif stream == "stderr":
            self.log_stderr(line_s)
            return self._parse_stderr(line_s.strip())
        else:
            raise RuntimeError(f"Unknown stream: {stream}")

    def _parse_stdout(self, _):
        return None

    def _parse_stderr(self, _):
        return None

    def finish(self, retcode):
        """Called when the tool finishes"""
        raise NotImplementedError(f"{self.__class__}.finish() not implemented")


class ProcessTask(Task):
    def __init__(self, cmd=None, timeout=None, name=None, envir=None, parser=None):
        super().__init__(timeout=timeout, name=name)
        self._env = get_env()
        self._proc = None
        # Here we store the `ProcessResult` in the default
        # implementation of `finish` method.
        self._retval = None
        # environment variables
        self._environ = envir
        self._cmd = cmd

        # while reading the output of the tool, we might not read whole lines
        # every time. Keep the partial output here. The key is either "stdout"
        # or "stderr"
        self._partial_output = {}
        self._output_parser = parser

    def env(self):
        return self._env

    def name(self):
        return self._name or str(self)

    def cmd(self):
        """
        The user can adjust what process is executed by setting self._cmd or by overwriting this method.

        Returns: A list that can be passed to Popen.
        """
        return self._cmd

    def set_cmd(self, cmd):
        self._cmd = cmd

    def is_done(self):
        return self._retval is not None

    def proc(self):
        return self._proc

    def parser(self):
        return self._output_parser

    def retval(self):
        return self._retval

    def fds(self):
        """Get stdout and stderr (in this order) filedescriptors"""
        assert self.proc(), "Process is not running"
        return self.proc().stdout.fileno(), self.proc().stderr.fileno()

    def cleanup(self):
        pass

    def execute(self):
        cmd = self.cmd()
        assert isinstance(cmd, list), cmd
        assert all(
            (isinstance(x, str) for x in cmd)
        ), f"cmd is not a list of strings: {cmd}"

        print_stdout(
            f"## Run {self.name()} [time: {self.start_time() - self.env().start_time}] with timeout {self.timeout()}",
            color="cyan",
        )
        print_stdout("#", " ".join(cmd))
        self._popen(cmd, self._environ)

    def finish(self):
        """
        The default implementation of `finish` method.
        """
        dbg(f"## End {self.name()}", color="cyan")

        if self._proc is None:
            return TaskResult("ERROR", descr="Process failed starting")

        retval = self._proc.wait()
        self._retval = ProcessResult(retval)
        if self._output_parser:
            res = self._output_parser.finish(retval)

        self._proc = None
        if retval == 0:
            return TaskResult("DONE")
        return TaskResult("ERROR")

    def _popen(self, cmd, env):
        self._proc = _popen(cmd, env)

    def stop(self):
        if self._proc:
            self._proc.send_signal(SIGINT)

    def kill(self):
        if self._proc:
            self._proc.terminate()
            self._proc.kill()
            # sometimes proc.kill() does not work :-/
            self._proc.send_signal(SIGKILL)

    def wait_for_finish(self, timeout=None):
        """
        Wait until the task finishes -- this blocks!

        This method can be used to synchronously wait until the task finishes.
        It should be used only in exceptional situations, with care, and ideally with a timeout.
        In the best case, you do not need to use this method. Where it make sense is, for example,
        when a task is stopped and it make take a few more miliseconds to actually finish
        and dump some files, then wait for the files.

        Params:
          timeout: None or float, timeout in miliseconds
        """
        if self._proc is None:
            return True

        if self._proc.poll() is not None:
            return True

        try:
            self._proc.wait(timeout)
            return True
        except TimeoutExpired:
            return False

    def is_running(self):
        return self._proc is not None

    def parse_output(self, line: bytes, stream: str):
        """
        Called for each line of the tool's output.

        Parameters:
          line: str, whole line received on the `stream`
          stream: str, either "stdout" or "stderr"

        Returns:
          A list of Result() objects if the result is known after
          parsing the current line, otherwise None.
        """
        if self._output_parser:
            return self._output_parser.parse(line.strip(), stream)
        return None

    def is_program(self):
        """
        Returns:

        True if this task is running a subprocess.
        (We need to know so that we can monitor its stdout and stderr).
        """
        return True
