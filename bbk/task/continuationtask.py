from bbk.dbg import dbg
from .task import Task

from .result import TaskResult


class ContinuationTask(Task):
    """
    This task runs a single task and upon completition
    runs a user function that determines how to continue.
    It is just a helper class to make things more nice,
    this functionality can be achieved also other ways.
    """

    def __init__(self, task, continuation=None, timeout=None, name=None, descr=None):
        super().__init__(timeout=timeout, name=name, descr=descr)
        assert task
        self._task = task
        self._result = None

        # overwrite the continuation method if given on commandline
        if continuation is not None:
            self.continuation = continuation

        self.listen_to(task)

    def task(self):
        return self._task

    def result(self):
        return self._result

    def continuation(self, result):
        """
        Override this method to take action after
        the task finishes. This is an alternative
        to passing a function as `continuation` parameter
        in the __init__ method.
        """
        raise RuntimeError("Must be overriden or given as a parameter")

    def execute(self):
        self._workflow.add_task(self._task)

    def subtask_finished(self, task, result):
        task.remove_listener(self)
        assert self._result is None, f"Already got a result: {self._result}"
        self._result = result

    def finish(self):
        """
        Run `continuation` and overwrite the result with the result
        from the continuation.
        """
        # do not proceed if the task was stopped
        if self.was_stopped():
            return TaskResult("ERROR", descr="Continuation task has been stopped")

        self._result = self.continuation(self._result)
        self._task = None
        return self._result

    def is_running(self):
        return self._start_time is not None and self._result is None

    def stop(self):
        if self._task:
            self._task.stop()

    def kill(self):
        # this is partially because of debugging
        assert self.was_stopped(), "Task must be stopped before trying to kill it"
        if self._task:
            self._task.kill()

    def is_done(self):
        return self._result is not None
