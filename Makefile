BUILD_KLEE_UCLIBC ?= "OFF"
KLEE_OPTIONS = #-DENABLE_TCMALLOC=OFF -DENABLE_SYSTEM_TESTS=OFF -DENABLE_UNIT_TESTS=OFF

ifeq ($(BUILD_KLEE_UCLIBC), ON)
KLEE_OPTIONS += -DKLEE_UCLIBC_PATH=../klee-uclibc
endif

all: mypy pylint

mypy:
	mypy bubaak bbk/

pylint:
	pylint bubaak bbk/

fixme:
	pylint --enable=fixme bubaak bbk/

# FORMATTING
black:
	black bubaak bbk/ --exclude slowbeast --exclude klee

venv-init:
	test -d venv/ || python3 -m venv venv/

venv: venv-init
	test -z "${VIRTUAL_ENV}" && echo "source venv/bin/activate"

autopep:
	autopep8 --in-place --aggressive --aggressive --recursive bubaak bbk


_build-and-copy: slowbeast-package build-klee
	mkdir -p build
	cd build && git init && git clean -xdf
	mkdir -p build/klee/build build/slowbeast
	cp -r slowbeast/dist/sb/* build/slowbeast
	cp -r klee/build/install/* build/klee/build
	cp -r bbk/ svcomp/ workflows/ build/
	cp bubaak build/
	cp LICENSE* README.md build/
	git rev-parse HEAD > build/git-version.txt


dist: _build-and-copy
	cd build && find . -name '*.so' | xargs strip
	cd build && find . -name '*.so.*' | xargs strip
	cd build && rm -rf bbk/__pycache__ bbk/tools/__pycache__
	cp svcomp/licenses/*LICENSE*.txt build/
	# check if we have more up to date licenses
	find ./venv -wholename '*site-packages/numpy*dist-info*/LICENSE.txt' -exec cp '{}' build/LICENSE.numpy.txt \;
	cd build && git add bubaak *LICENSE* README.md svcomp/*.py bbk/ workflows/
	cd build && git add git-version.txt
	cd build/slowbeast &&\
		git add sb &&\
		if [ -d _internal ]; then cd _internal; fi\
		&& git add libpython* base_library.zip lib-dynload/ llvmlite/ numpy* z3/
	cd build/klee/build && strip bin/klee
	cd build/klee/build && git add bin/klee bin/ktest-tool lib/
	# splitter
	test -d program-splitter && cd program-splitter &&\
		bash ./deploy.sh && cp archives/splitter.zip ../build
	test -d program-splitter && cd build &&\
		unzip splitter.zip && mv splitter/ program-splitter &&\
		cd program-splitter && git add lib/ *.py
	# cleanup unwanted things
	cd build && git commit -a --allow-empty -m 'Update build' && git clean -xdf

setup: slowbeast-llvmlite build-klee
	pip install z3-solver --upgrade


archive: dist
	cd build && git archive --prefix "bubaak/" -o ../bubaak.zip -9 --format zip HEAD



### ---------------------------
#    SlowBeast
### ---------------------------

slowbeast-llvmlite:
	test -d slowbeast/llvmlite || (cd slowbeast && git clone https://github.com/mchalupa/llvmlite)
	cd slowbeast/llvmlite && python3 ./setup.py build

slowbeast-package: slowbeast-llvmlite
	make pyinstaller -C slowbeast


### ---------------------------
#    KLEE
### ---------------------------
build-klee-uclibc:
ifeq ($(BUILD_KLEE_UCLIBC), ON)
	test -d klee/klee-uclibc || (cd klee && git clone https://github.com/klee/klee-uclibc)
	test -f klee/klee-uclibc/config.log || (cd klee/klee-uclibc && ./configure --make-llvm-lib)
	make -C klee/klee-uclibc -j4 KLEE_CFLAGS="-DKLEE_SYM_PRINTF"
endif

build-klee: build-klee-uclibc
	mkdir -p klee/build
	test -f klee/build/CMakeCache.txt ||\
		(cd klee/build && cmake .. -DCMAKE_INSTALL_PREFIX=./install -DENABLE_POSIX_RUNTIME=ON $(KLEE_OPTIONS))
	make -j4 -C klee/build
	make install -C klee/build


### ---------------------------
#    SPLITING
### ---------------------------

splitting-init:
	test -d program-splitter || git clone https://gitlab.com/cedricrupb/program-splitter.git


splitting-deploy: splitting-init
	cd program-splitter; bash ./install.sh

splitting: splitting-deploy

.PHONY: splitting-init

