from tempfile import mktemp
from bbk.compiler import CompilerTask, CompilationOptions
from bbk.task.continuationtask import ContinuationTask
from bbk.task.aggregatetask import AggregateTask
from bbk.task.result import TaskResult
from bbk.result import Result
from bbk.env import get_env

from bbk.tools.klee import Klee, get_klee_args
from bbk.tools.slowbeast import SlowBeast, get_slowbeast_args
from bbk.workflow import Workflow


def task_result_is_conclusive(result):
    return result.is_done() and (any((r.is_incorrect() for r in result.output)) or all((r.is_correct() for r in result.output)))

from .default import CompileAndCheck, VerificationData, task_result_is_conclusive, found_bug

class CooperativeSeBself(AggregateTask):
    """
    Start SE and BSELF in SlowBeast in parallel with cooperation.
    """

    def __init__(self, bitcode, data):
        super().__init__([], name="CoopSlowBeast")
        self._bitcode = bitcode
        self.data = data

    def execute(self):
        chan = mktemp(dir=get_env().workdir, prefix="chan-")

        self.add_subtask(
            SlowBeast(self._bitcode,
                 self.data.properties,
                 get_slowbeast_args(self.data.args, self.data.properties,
                                    [f"-coop-channel=w:{chan}"]))
        )

        self.add_subtask(
            SlowBeast(self._bitcode,
                 self.data.properties,
                 get_slowbeast_args(self.data.args, self.data.properties,
                                    ["-bself",
                                     f"-coop-channel=r:{chan}"]))
        )

    def aggregate(self, task, result):
        if task_result_is_conclusive(result):
            return result
        return None


def create_task(programs, args, properties):
    """
    The default workflow for Bubaak. The initial task is to compile the
    programs into LLVM and then run LEE and SlowBeast in parallel on the
    compiled program.
    """

    if "cooperative" in args.D:
        task = ContinuationTask(
                   CompileAndCheck(programs,
                                   properties,
                                   include_dirs=args.I),
                   continuation=lambda result:
                   TaskResult("REPLACE_TASK",
                       CooperativeSeBself(
                               result.output,
                               VerificationData(programs, properties, args)
                       )
                   )
                   # Compilation succeeded and bug was not found, so start verifiers
                   if result.is_done()
                   else result,
                   descr="Compile and Start Slowbeast",
            )
    else:
        task = ContinuationTask(
                   CompileAndCheck(programs,
                                   properties,
                                   include_dirs=args.I),
                   continuation=lambda result:
                   TaskResult("REPLACE_TASK",
                       SlowBeast(
                               result.output,
                               properties,
                               args=get_slowbeast_args(args, properties, args.X),
                               name="SlowBeast"
                       )
                   )
                   # Compilation succeeded and bug was not found, so start verifiers
                   if result.is_done()
                   else result,
                   descr="Compile and Start Slowbeast",
            )

    timeout = None
    if args.timeout is not None:
        timeout = args.timeout

    # run the verification task inside another task so that the "super"
    # task has a stable timeout (the verifiers have their own timeouts
    # and they get "reset" whenever the task is replaced)
    return AggregateTask(
        [task],
        aggregate=lambda task, result: result,
        timeout=timeout
    )

    return init


def workflow(programs, args, properties):
    return Workflow([create_task(programs, args, properties)])

