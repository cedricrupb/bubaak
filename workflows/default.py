from tempfile import mktemp

from bbk.compiler import CompilerTask, CompilationOptions
from bbk.task.continuationtask import ContinuationTask
from bbk.task.aggregatetask import AggregateTask
from bbk.task.result import TaskResult
from bbk.result import Result
from bbk.env import get_env

from bbk.tools.klee import Klee, get_klee_args
from bbk.tools.slowbeast import SlowBeast, get_slowbeast_args
from bbk.workflow import Workflow

def found_bug(result):
    return isinstance(result.output, list) and any((isinstance(r, Result) and r.is_incorrect()) for r in result.output)

def task_result_is_conclusive(result):
    return result.is_done() and (any((r.is_incorrect() for r in result.output)) or all((r.is_correct() for r in result.output)))

class VerificationData:
    def __init__(self, inputs, properties, args):
        self.inputs = inputs
        self.properties = properties
        self.args = args

class CooperativeSeBself(AggregateTask):
    """
    Start SE and BSELF in SlowBeast in parallel with cooperation.
    """

    def __init__(self, bitcode, data):
        super().__init__([], name="CoopSlowBeast")
        self._bitcode = bitcode
        self.data = data

    def execute(self):
        chan = mktemp(dir=get_env().workdir, prefix="chan-")

        self.add_subtask(
            SlowBeast(self._bitcode,
                 self.data.properties,
                 get_slowbeast_args(self.data.args, self.data.properties,
                                    [f"-coop-channel=w:{chan}"]))
        )

        self.add_subtask(
            SlowBeast(self._bitcode,
                 self.data.properties,
                 get_slowbeast_args(self.data.args, self.data.properties,
                                    ["-bself",
                                     f"-coop-channel=r:{chan}"]))
        )

    def aggregate(self, task, result):
        if task_result_is_conclusive(result):
            return result
        return None

    def finish(self):
        result = self.result()
        if result is None:
            return TaskResult("DONE", output=[Result(Result.UNKNOWN, None, "No config got a result")])
        return result



class CompileAndCheck(CompilerTask):
    def __init__(self, inputs, properties, include_dirs=None, options=None):
        options = options or CompilationOptions()
        if any((p.is_no_signed_overflow() for p in properties)):
            options._sanitize.append("ubsan")
        if any((p.is_valid_deref() for p in properties)):
            options._sanitize.append("asan")

        super().__init__(inputs, include_dirs, options)
        self._properties = properties

    def continuation(self, result):
        warnings = self.task().warnings()
        if warnings:
            overflow = self._properties.get("no-signed-overflow")
            if overflow:
                for line in warnings:
                    if "warning: overflow in expression;" in line or (
                        "implicit conversion" in line and "changes value from" in line and "to 'float'" not in line
                    ):
                        # This is a work-around for the problem that the translation to LLVM looses
                        # some information -- in this case, an overflow is detected by clang, reported,
                        # but LLVM is generated without this overflow
                        return TaskResult(
                            "DONE",
                            [
                                Result(
                                    Result.INCORRECT,
                                    overflow,
                                    "Signed overflow detected",
                                )
                            ],
                        )

        return super().continuation(result)


class StartTerminationVerification(AggregateTask):
    """
    Start KLEE and SlowBeast in parallel for verifying termination
    """

    def __init__(self, bitcode, data):
        super().__init__([], name="StartTerminationVerification")
        self._bitcode = bitcode
        self.data = data

    def execute(self):
        self.add_subtask(
            Klee(self._bitcode,
                 self.data.properties,
                 get_klee_args(self.data.args, self.data.properties))
        )

        self.add_subtask(
            SlowBeast(self._bitcode,
                 self.data.properties,
                 get_slowbeast_args(self.data.args, self.data.properties))
        )

    def aggregate(self, task, result):
        if task_result_is_conclusive(result):
            return result
        return None



class StartVerificationKlee(ContinuationTask):
    def __init__(self, bitcode, data):
        to = data.args.timeout
        if to is not None and to > 0:
            klee_timeout = max(int(to / 3), 10)
        else:
            klee_timeout = 123

        super().__init__(Klee(bitcode, data.properties,
                              get_klee_args(data.args, data.properties),
                              timeout=klee_timeout),
                         name="StartVerificationKlee")
        self._bitcode = bitcode
        self.data = data

    def klee_failed_on_floats(self):
        for r in self.task().parser().killed_paths():
            if "silently concretizing (reason: floating point)" in r.info():
                return True
        return False

    def continuation(self, result):
        if task_result_is_conclusive(result):
            return result

        properties = self.data.properties

        if self.klee_failed_on_floats():
            args = get_slowbeast_args(self.data.args, properties)
            task = SlowBeast(self._bitcode,
                             self.data.properties,
                             args=args,
                             name="sb-se"
                    )
            return TaskResult("REPLACE_TASK", task)


        if any((p.is_no_signed_overflow() for p in properties)) or\
           any((p.is_valid_deref() for p in properties)):
            # we need to recompile the input file, because KLEE used sanitizers
            task = ContinuationTask(
                        CompileAndCheck(self.data.inputs,
                                        self.data.properties,
                                        include_dirs=self.data.args.I),
                        continuation=lambda result:
                        TaskResult("REPLACE_TASK",
                            CooperativeSeBself (
                                    result.output,
                                    self.data,
                            )
                        )
                        # Compilation succeeded and bug was not found, so start verifiers
                        if result.is_done()
                        else result,
                        descr="Compile and Start Slowbeast",
                     )
        else:
            task = CooperativeSeBself (
                           self._bitcode,
                           self.data,
                   )

        return TaskResult("REPLACE_TASK", task)



def create_task(programs, args, properties):
    """
    The default workflow for Bubaak. The initial task is to compile the
    programs into LLVM and then run LEE and SlowBeast in parallel on the
    compiled program.
    """

    if len(properties) == 1 and properties[0].is_termination():
        init = ContinuationTask(
           CompilerTask(programs, include_dirs=args.I),
           continuation=lambda result: TaskResult(
               "REPLACE_TASK",
               StartTerminationVerification(result.output,
                             VerificationData(programs, properties, args)),
           )
           # Compilation succeeded and bug was not found, so start verifiers
           if result.is_done()
           else result,
           descr="Compile and Start Verification",
        )
    else:
        init = ContinuationTask(
           CompileAndCheck(programs, properties, include_dirs=args.I),
           continuation=lambda result: TaskResult(
               "REPLACE_TASK",
               StartVerificationKlee(result.output,
                             VerificationData(programs, properties, args)),
           )
           # Compilation succeeded and bug was not found, so start verifiers
           if result.is_done() and not found_bug(result)
           else result,
           descr="Compile and Start Verification",
        )

    timeout = None
    if args.timeout is not None:
        timeout = args.timeout

    # run the verification task inside another task so that the "super"
    # task has a stable timeout (the verifiers have their own timeouts
    # and they get "reset" whenever the task is replaced)
    return AggregateTask(
        [init],
        aggregate=lambda task, result: result,
        timeout=timeout
    )

    return init


def workflow(programs, args, properties):
    return Workflow([create_task(programs, args, properties)])

